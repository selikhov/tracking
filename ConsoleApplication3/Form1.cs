﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bespoke.Common;
using Bespoke.Common.Osc;
using System.Net;
using System.Globalization;

namespace Receiver
{
    public partial class Form1 : Form
    {
        
        private static readonly int Port = 8080;
        private static readonly string Accel = "/gyrosc/qweerty/accel";
        private static readonly string Gyro = "/gyrosc/qweerty/gyro";

        private static int sBundlesReceivedCount;
        private static int sMessagesReceivedCount;

        private OscServer oscServer;
        private static Form1 _instance;
        private Sensors sensorInstance;

		public double x, y, z;

        static public Form1 getInstance()
        {
            if (_instance == null)
                _instance = new Form1();

            return _instance;
        }

        public Form1()
        {
            _instance = this;
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 MainForm = new Form2("acc");
            MainForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Sensors sensor = Sensors.getInstance();
            sensor.clearData();
            //oscServer.Stop();
            //TimeSpan timeDiff = DateTime.Now - sensorInstance.timeStart;
            //Console.WriteLine("Server stop... Time({0}), Packets({1})", timeDiff.ToString(), sensorInstance.packet_num);
            //System.IO.File.WriteAllText(@"E:\Dropbox\OscOutput_CalInertialAndMag.csv", sensorInstance.csv.ToString());


        }

        private static void oscServer_BundleReceived(object sender, OscBundleReceivedEventArgs e)
        {
            sBundlesReceivedCount++;
            OscBundle bundle = e.Bundle;
            //Console.WriteLine(string.Format("\nBundle Received [{0}:{1}]: Nested Bundles: {2} Nested Messages: {3}", bundle.SourceEndPoint.Address, bundle.TimeStamp, bundle.Bundles.Count, bundle.Messages.Count));
            //Console.WriteLine("Total Bundles Received: {0}", sBundlesReceivedCount);
        }

        private static void oscServer_MessageReceived(object sender, OscMessageReceivedEventArgs e)
        {
            sMessagesReceivedCount++;

            Form1 Form1Instance = getInstance();
            OscMessage message = e.Message;
            Sensors sensor = Sensors.getInstance();
            sensor.HandleOSCMessage(e.Message);
            if (sensor.is_valid) {
                Accelerometer Accel = sensor.GetAccelObject();
                Gyroscope Gyro = sensor.GetGyroscopeObject();
                Compas Compas = sensor.GetCompasObject();

                sensor.packet_num++;
                CultureInfo ci = new CultureInfo("en-US");
                string newline = string.Format(
                    "{0},{1},{2},{3}," + 
                    "{4},{5},{6}," + 
                    "{7},{8},{9}{10}",
                    sensor.packet_num, Gyro.x.ToString("G", ci), Gyro.y.ToString("G", ci), Gyro.z.ToString("G", ci),
                    Accel.x.ToString("G", ci), Accel.y.ToString("G", ci), Accel.z.ToString("G", ci),
                    Compas.x.ToString("G", ci), Compas.y.ToString("G", ci), Compas.z.ToString("G", ci), Environment.NewLine
                );
                sensor.csv.Append(newline);
                //sensor.Accel.clear();
            }

            double x, y, z;
            x = message.Data[0] is byte[] ? BitConverter.ToDouble((byte[])message.Data[0], 0) : Double.Parse(message.Data[0].ToString());
            y = message.Data[1] is byte[] ? BitConverter.ToDouble((byte[])message.Data[1], 0) : Double.Parse(message.Data[1].ToString());
            z = message.Data[2] is byte[] ? BitConverter.ToDouble((byte[])message.Data[2], 0) : Double.Parse(message.Data[2].ToString());

            //x = Math.Round(x, 4);
            //y = Math.Round(y, 4);
            //z = Math.Round(z, 4);
            
			switch ("qq")//message.Address)
            {
                case "/gyrosc/grav":
                    Form1Instance.label1.Text = x.ToString("G");
                    Form1Instance.label4.Text = y.ToString("G");
                    Form1Instance.label6.Text = z.ToString("G");
                    break;
            }

        }

        private static void oscServer_ReceiveErrored(object sender, ExceptionEventArgs e)
        {
            Console.WriteLine("Error during reception of packet: {0}", e.Exception.Message);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 MainForm = new Form2("linvel");
            MainForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 MainForm = new Form2("vel");
            MainForm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form2 MainForm = new Form2("quat_math");
            MainForm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form2 MainForm = new Form2("quat_sensor");
            MainForm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form3 FftForm = new Form3("fft_real");
            FftForm.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Form3 FftForm = new Form3("fft_complex");
            FftForm.Show();
        }
    }
}
