﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bespoke.Common.Osc;


using System.Data;
using System.Drawing;
using Bespoke.Common;
using System.Net;
using System.Globalization;

namespace Receiver
{
    class Sensors
    {
        static Sensors _instance = null;

		//Object sensors fields
        public Accelerometer Accel;
        private Gyroscope Gyro;
        private Compas Compas;
		private Quaternion Quat;
        public TimeSpan time_diff;
        public DateTime timeStart;

		//OSC Message fields
		private static readonly int Port = 8080;
		private static int sBundlesReceivedCount;
		private static int sMessagesReceivedCount;
		private static OscServer oscServer;

		//Public test field
		public double x, y, z;
        private static MahonyAHRS AHRSfilt;
        
        public float[] filtLinVel = { 0f, 0f, 0f };
        public float[] linVel = { 0f, 0f, 0f };
        public float[] vel = { 0f, 0f, 0f };

        private DateTime datetime = DateTime.Now;
        public double time = 0;

        public bool is_valid;
        public StringBuilder csv = new StringBuilder();
        public int packet_num = 0;

		public List<double> dataAccXList = new List<double>();
		public List<double> dataAccYList = new List<double>();
		public List<double> dataAccZList = new List<double>();

        static public Sensors getInstance(){
            if(_instance == null)
                _instance = new Sensors();
                
            return _instance;
        }

		//OSC metod for init Lisen Message
		private static void ListenOSCStart()
		{
			oscServer = new OscServer(TransportType.Udp, IPAddress.Any, Port);

			oscServer.FilterRegisteredMethods = false;
			//для обработки конкретных пакетов
			//oscServer.RegisterMethod(Accel);
			//oscServer.RegisterMethod(Gyro);

			//дополнительные обработчики
			oscServer.BundleReceived += new EventHandler<OscBundleReceivedEventArgs>(oscServer_BundleReceived);
			oscServer.MessageReceived += new EventHandler<OscMessageReceivedEventArgs>(oscServer_MessageReceived);	
			oscServer.ReceiveErrored += new EventHandler<ExceptionEventArgs>(oscServer_ReceiveErrored);


			oscServer.ConsumeParsingExceptions = false;

			oscServer.Start();

			Console.WriteLine("Server start...");
		}

		//OSC Message handler
		private static void oscServer_MessageReceived(object sender, OscMessageReceivedEventArgs e)
		{
			int i = 0;
			Sensors _this = getInstance ();
			sMessagesReceivedCount++;
			OscMessage message = e.Message;

			_this.HandleOSCMessage(e.Message);

			Accelerometer acc = _this.GetAccelObject();
			acc.customMedianFilter();
            //Gyroscope gyro = _this.GetGyroscopeObject();
            //Compas compas = _this.GetCompasObject();
            //Quaternion quat = _this.GetQuaternion();
            //_this._setTimeDiff();
            //Form1 form = Form1.getInstance();
            //form.label1.Text = (1000 / _this.time_diff.Milliseconds).ToString();
            float[] tcc = { (float)acc._acc_x, (float)acc._acc_y, (float)acc._acc_z };

			float[] dataAcc = { (float)acc._acc_x, (float)acc._acc_y, (float)acc._acc_z };
			_this.dataAccXList.Add(acc._acc_x);
			_this.dataAccYList.Add(acc._acc_y);
			_this.dataAccZList.Add(acc._acc_z);

            //получаем линейную скорость (интегрируем ускорение)
            _this.vel = AHRSfilt.CalculateLinearVelocyti(tcc);
            //стбилизируем скорость
            _this.vel = AHRSfilt.stationarStabilize(_this.vel[0], _this.vel[1], _this.vel[2], (float)acc._acc_x, (float)acc._acc_y, (float)acc._acc_z, 0.02f);

            _this.linVel = AHRSfilt.CalculateLinPos(_this.vel);

            _this.filtLinVel = _this.linVel;
            //_this.filtLinVel[0] = AHRSfilt.filterloop(_this.linVel[0], 0);
            //_this.filtLinVel[1] = AHRSfilt.filterloop(_this.linVel[1], 1);
            //_this.filtLinVel[2] = AHRSfilt.filterloop(_this.linVel[2], 2);

            //отправляем данные в Unity
            UdpSocket.Send(_this.filtLinVel);
		}

		private static void oscServer_BundleReceived(object sender, OscBundleReceivedEventArgs e)
		{
			sBundlesReceivedCount++;
			OscBundle bundle = e.Bundle;
			//Console.WriteLine(string.Format("\nBundle Received [{0}:{1}]: Nested Bundles: {2} Nested Messages: {3}", bundle.SourceEndPoint.Address, bundle.TimeStamp, bundle.Bundles.Count, bundle.Messages.Count));
			//Console.WriteLine("Total Bundles Received: {0}", sBundlesReceivedCount);
		}

		private static void oscServer_ReceiveErrored(object sender, ExceptionEventArgs e)
		{
			Console.WriteLine("Error during reception of packet: {0}", e.Exception.Message);
		}

        private Sensors() 
        {
            Accel = new Accelerometer();
            Gyro = new Gyroscope();
            Compas = new Compas();
			Quat = new Quaternion ();
            AHRSfilt = new MahonyAHRS(1f / 200f);
            ListenOSCStart();

        }

        public void HandleOSCMessage(OscMessage message)
        {
            switch(message.Address){
                case "/gyrosc/grav":
                case "/gyrosc/accel":
					Accel.SetValueFromOscMessageWithGrav(message.Data, message.Address);
                    break;
                case "/gyrosc/rrate":
                    Gyro.SetValueFromOscMessage(message.Data);
                    break;
                case "/gyrosc/mag":
                    Compas.SetValueFromOscMessage(message.Data);
                    break;
				case "/gyrosc/quat":
					Quat.SetValueFromOscMessage(message.Data);
					break;
            }
            this.is_valid = (!this.Accel.is_read && !this.Gyro.is_read && !this.Compas.is_read);
        }

        public Accelerometer GetAccelObject()
        {
            Accel.is_read = true;
            return Accel;

        }

        public Gyroscope GetGyroscopeObject()
        {
            this.Gyro.is_read = true;

            return this.Gyro;
        }

        public Compas GetCompasObject()
        {
            this.Compas.is_read = true;

            return this.Compas;
        }

		public Quaternion GetQuaternion()
		{
			Quat.is_read = true;

			return Quat;
		}

        private void _setTimeDiff(){
            time_diff = DateTime.Now - datetime;
            datetime = DateTime.Now;
        }

        public void clearData(){
            filtLinVel[0] = 0f;
            filtLinVel[1] = 0f;
            filtLinVel[2] = 0f;
        }
    }
}
