using System;
using System.Net;
using System.Collections.Generic;
using Bespoke.Common;
using Bespoke.Common.Osc;

using System.Windows.Forms;

namespace Receiver
{
    public enum DemoType
    {
        Udp,
        Tcp,
        Multicast
    }

	public class Program
	{
        public static void Main() {
            //Form1 MainForm = new Form1();
            //MainForm.ShowDialog();

			//Application.Run (new MainForm ());
			Application.Run (new Form1 ());
			//Application.Run (new Form2 ("quat"));
			//Application.Run (new Form2 ("accel"));
        }


		public static void Main1(string[] args)
		{
            OscServer oscServer;
            DemoType demoType = GetDemoType();
            switch (demoType)
            {
                case DemoType.Udp:
                    oscServer = new OscServer(TransportType.Udp, IPAddress.Any, Port);
                    break;

                case DemoType.Tcp:
                    oscServer = new OscServer(TransportType.Tcp, IPAddress.Any, Port);
                    break;

                case DemoType.Multicast:
                    oscServer = new OscServer(IPAddress.Parse("224.25.26.27"), Port);
                    break;

                default:
                    throw new Exception("Unsupported receiver type.");
            }
            
            oscServer.FilterRegisteredMethods = false;
			oscServer.RegisterMethod(Accel);
            oscServer.RegisterMethod(Gyro);
            oscServer.BundleReceived += new EventHandler<OscBundleReceivedEventArgs>(oscServer_BundleReceived);
			oscServer.MessageReceived += new EventHandler<OscMessageReceivedEventArgs>(oscServer_MessageReceived);
            oscServer.ReceiveErrored += new EventHandler<ExceptionEventArgs>(oscServer_ReceiveErrored);
            oscServer.ConsumeParsingExceptions = false;

            oscServer.Start();

            string ResultLine = demoType.ToString();
            Console.WriteLine("Osc Receiver: " + ResultLine);

			Console.WriteLine("Press any key to exit.");
			Console.ReadKey();
			oscServer.Stop();
		}

        private static DemoType GetDemoType()
        {
            Dictionary<ConsoleKey, DemoType> keyMappings = new Dictionary<ConsoleKey, DemoType>();
            keyMappings.Add(ConsoleKey.D1, DemoType.Udp);
            keyMappings.Add(ConsoleKey.D2, DemoType.Tcp);
            keyMappings.Add(ConsoleKey.D3, DemoType.Multicast);

            Console.WriteLine("\nWelcome to the Bespoke Osc Receiver Demo.\nPlease select the type of receiver you would like to use:");
            Console.WriteLine("  1. Udp\n  2. Tcp\n  3. Udp Multicast");

            ConsoleKeyInfo key = Console.ReadKey();
            while (keyMappings.ContainsKey(key.Key) == false)
            {
                Console.WriteLine("\nInvalid selection\n");
                Console.WriteLine("  1. Udp\n  2. Tcp\n  3. Udp Multicast");
                key = Console.ReadKey();
            }

            Console.Clear();

            return keyMappings[key.Key];
        }

        private static void oscServer_BundleReceived(object sender, OscBundleReceivedEventArgs e)
        {
            sBundlesReceivedCount++;
            OscBundle bundle = e.Bundle;
            //Console.WriteLine(string.Format("\nBundle Received [{0}:{1}]: Nested Bundles: {2} Nested Messages: {3}", bundle.SourceEndPoint.Address, bundle.TimeStamp, bundle.Bundles.Count, bundle.Messages.Count));
            //Console.WriteLine("Total Bundles Received: {0}", sBundlesReceivedCount);
        }

		private static void oscServer_MessageReceived(object sender, OscMessageReceivedEventArgs e)
		{
            sMessagesReceivedCount++;
            OscMessage message = e.Message;
            Sensors sensor = Sensors.getInstance();
            sensor.HandleOSCMessage(e.Message);
            Accelerometer accel = sensor.GetAccelObject();



            if (sensor.is_valid)
            {
                //TrajectoryGenerator Gen = TrajectoryGenerator.getInstance(sensor);
                //Gen.MathTraectory();
                //Console.WriteLine(string.Format("Accel {0}\t{1}\t{2}\t\t{3}", Gen.point.x, Gen.point.y, Gen.point.z, sensor.time));
                Console.WriteLine(string.Format("Accel {0}\t\t{1}\t\t{2}", accel.x * 100, accel.y * 100, accel.z*100));
            }
            

            //Console.WriteLine(string.Format("\nMessage Received [{0}]: {1}", message.SourceEndPoint.Address, message.Address));
            //Console.WriteLine(string.Format("Message contains {0} objects.", message.Data.Count));
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"E:\OscOutput.txt", true)){
            //    file.WriteLine(string.Format("\nMessage Received [{0}]: {1}", message.SourceEndPoint.Address, message.Address));
            //}

            /*for (int i = 0; i < message.Data.Count; i++)
            {
                string dataString;

                if (message.Data[i] == null){
                    dataString = "Nil";
                }
                else{
                    dataString = (message.Data[i] is byte[] ? BitConverter.ToString((byte[])message.Data[i]) : message.Data[i].ToString());
                }

                double MessageVal = Double.Parse(dataString);
                double ResultValue = Double.Parse(dataString) * 100;
                Console.Write(string.Format("[{0}] \t", String.Format("{0,8:0.000}", ResultValue)));
                Console.Write(string.Format("[{0}] \t", dataString));
            }*/

            //Console.WriteLine("Total Messages Received: {0}", sMessagesReceivedCount);
            //Console.WriteLine();
		}

        private static void oscServer_ReceiveErrored(object sender, ExceptionEventArgs e)
        {
            Console.WriteLine("Error during reception of packet: {0}", e.Exception.Message);
        }

		private static readonly int Port = 5103;
        private static readonly string Accel = "/gyrosc/qweerty/accel";
        private static readonly string Gyro = "/gyrosc/qweerty/gyro";

        private static int sBundlesReceivedCount;
        private static int sMessagesReceivedCount;
	}
}
