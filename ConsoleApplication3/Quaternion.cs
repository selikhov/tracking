﻿using System;
using System.Collections.Generic;

namespace Receiver
{

	public class Quaternion
	{	public bool is_read = true;
		public double q;
		public double x;
		public double y;
		public double z;
		public void SetValueFromOscMessage(IList<object> Data)
		{
			q = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[0], 0) : Double.Parse(Data[0].ToString());
			x = Data[1] is byte[] ? BitConverter.ToDouble((byte[])Data[1], 0) : Double.Parse(Data[1].ToString());
			y = Data[2] is byte[] ? BitConverter.ToDouble((byte[])Data[2], 0) : Double.Parse(Data[2].ToString());
			z = Data[3] is byte[] ? BitConverter.ToDouble((byte[])Data[3], 0) : Double.Parse(Data[3].ToString());
			is_read = false;
		}
	}
}
