﻿using System;
using System.Collections.Generic;

namespace Receiver
{
	public class Compas
	{
		public bool is_read = true;
		public double x;
		public double y;
		public double z;

		public void SetValueFromOscMessage(IList<object> Data)
		{
			this.x = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[0], 0) : Double.Parse(Data[0].ToString());
			this.y = Data[1] is byte[] ? BitConverter.ToDouble((byte[])Data[1], 0) : Double.Parse(Data[1].ToString());
			this.z = Data[2] is byte[] ? BitConverter.ToDouble((byte[])Data[2], 0) : Double.Parse(Data[2].ToString());
			this.is_read = false;
		}

	}
}

