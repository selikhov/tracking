﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Receiver
{

	public class Accelerometer
	{
		public bool is_read = true;
		public double x = 0;
		public double y = 0;
		public double z = 0;

		public double _acc_x = 0;
		public double _acc_y = 0;
		public double _acc_z = 0;

		public double _grav_x = 0;
		public double _grav_y = 0;
		public double _grav_z = 0;

		private bool accel_set = false;
		private bool grav_set = false;

		private Queue<double> QuFiltX = new Queue<double>(9);
		private Queue<double> QuFiltY = new Queue<double>(9);
		private Queue<double> QuFiltZ = new Queue<double>(9);

        private float g = 9.8066f;

		public void SetValueFromOscMessage(IList<object> Data, string Address)
		{
			double tmp_x = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[0], 0) : Double.Parse(Data[0].ToString());
			double tmp_y = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[1], 0) : Double.Parse(Data[1].ToString());
			double tmp_z = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[2], 0) : Double.Parse(Data[2].ToString());

			switch (Address) {
				case "/gyrosc/accel":
					x = tmp_x;
					y = tmp_y;
					z = tmp_z;
					accel_set = true;
				break;
			}

			is_read = false;
		}

		public void SetValueFromOscMessageWithGrav(IList<object> Data, string Address)
		{
			double tmp_x = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[0], 0) : Double.Parse(Data[0].ToString());
			double tmp_y = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[1], 0) : Double.Parse(Data[1].ToString());
			double tmp_z = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[2], 0) : Double.Parse(Data[2].ToString());

            //tmp_x = tmp_x * g;
            //tmp_y = tmp_y * g;
            //tmp_z = tmp_z * g;

			switch (Address) {
			case "/gyrosc/accel":
				_acc_x = tmp_x;
				_acc_y = tmp_y;
				_acc_z = tmp_z;
				accel_set = true;
				break;
			case "/gyrosc/grav":
				_grav_x = tmp_x;
				_grav_y = tmp_y;
				_grav_z = tmp_z;
				grav_set = true;
				break;
			}

			if (accel_set && grav_set) {
				x = _acc_x + _grav_x;
				y = _acc_y + _grav_y;
				z = _acc_z + _grav_z;
				accel_set = false;
				grav_set = false;
				is_read = false;
			}
		}
			

//		public float[] customMedianFilter()
//		{
//			float[] res = new float[3];
//			float[] val = {(float)x, (float)y, (float)z };
//			QuFiltX.Enqueue (val[0]);
//			QuFiltY.Enqueue (val[1]);
//			QuFiltZ.Enqueue (val[2]);
//
//			if (QuFiltX.Count () == 5) {
//				float[] ListX = QuFiltX.ToArray();
//				float[] ListY = QuFiltY.ToArray();
//				float[] ListZ = QuFiltZ.ToArray();
//
//				Array.Sort(ListX);
//				Array.Sort(ListY);
//				Array.Sort(ListZ);
//
//				res[0] = ListX [2];
//				res[1] = ListY [2];
//				res[2] = ListZ [2];
//			} else {
//				res = val;
//			}
//			return res;
//		}

		public void customMedianFilter()
		{
			QuFiltX.Enqueue (_acc_x);
			QuFiltY.Enqueue (_acc_y);
			QuFiltZ.Enqueue (_acc_z);

			if (QuFiltX.Count() == 9) {
				double[] ListX = QuFiltX.ToArray();
				double[] ListY = QuFiltY.ToArray();
				double[] ListZ = QuFiltZ.ToArray();

				//Array.Sort(ListX);
				//Array.Sort(ListY);
				//Array.Sort(ListZ);

//				x = ListX [3];
//				y = ListY [3];
//				z = ListZ [3];
				x = ListX.Sum() / 9;
				y = ListY.Sum() / 9;
				z = ListZ.Sum() / 9;
			} 
		}

	}

}

