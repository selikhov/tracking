﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Receiver
{
    /// <summary>
    /// MahonyAHRS class. Madgwick's implementation of Mayhony's AHRS algorithm.
    /// </summary>
    /// <remarks>
    /// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
    /// </remarks>
    public class MahonyAHRS
    {
        /// <summary>
        /// Gets or sets the sample period.
        /// </summary>
        public float SamplePeriod { get; set; }

        /// <summary>
        /// Gets or sets the algorithm proportional gain.
        /// </summary>
        public float Kp { get; set; }

        /// <summary>
        /// Gets or sets the algorithm integral gain.
        /// </summary>
        public float Ki { get; set; }

        /// <summary>
        /// Gets or sets the Quaternion output.
        /// </summary>
        public float[] Quaternion { get; set; }

        /// <summary>
        /// Gets or sets the integral error.
        /// </summary>
        private float[] eInt { get; set; }

        private float[] prevLinVel = {0f, 0f, 0f};
        private float[] prevLinVelHP = {0f, 0f, 0f};
        
		private float[] linVel = {0f, 0f, 0f};

        private float[,] xv = new float[,] { { 0f, 0f }, { 0f, 0f }, { 0f, 0f } };
        private float[,] yv = new float[,] { { 0f, 0f }, { 0f, 0f }, { 0f, 0f } };

        /// <summary>
        /// Initializes a new instance of the <see cref="MadgwickAHRS"/> class.
        /// </summary>
        /// <param name="samplePeriod">
        /// Sample period.
        /// </param>
        public MahonyAHRS(float samplePeriod)
            : this(samplePeriod, 1f, 0f)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MadgwickAHRS"/> class.
        /// </summary>
        /// <param name="samplePeriod">
        /// Sample period.
        /// </param>
        /// <param name="kp">
        /// Algorithm proportional gain.
        /// </param> 
        public MahonyAHRS(float samplePeriod, float kp)
            : this(samplePeriod, kp, 0f)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MadgwickAHRS"/> class.
        /// </summary>
        /// <param name="samplePeriod">
        /// Sample period.
        /// </param>
        /// <param name="kp">
        /// Algorithm proportional gain.
        /// </param>
        /// <param name="ki">
        /// Algorithm integral gain.
        /// </param>
        public MahonyAHRS(float samplePeriod, float kp, float ki)
        {
            SamplePeriod = samplePeriod;
            Kp = kp;
            Ki = ki;
            Quaternion = new float[] { 1f, 0f, 0f, 0f };
            eInt = new float[] { 0f, 0f, 0f };
        }

        /// <summary>
        /// Algorithm AHRS update method. Requires only gyroscope and accelerometer data.
        /// </summary>
        /// <param name="gx">
        /// Gyroscope x axis measurement in radians/s.
        /// </param>
        /// <param name="gy">
        /// Gyroscope y axis measurement in radians/s.
        /// </param>
        /// <param name="gz">
        /// Gyroscope z axis measurement in radians/s.
        /// </param>
        /// <param name="ax">
        /// Accelerometer x axis measurement in any calibrated units.
        /// </param>
        /// <param name="ay">
        /// Accelerometer y axis measurement in any calibrated units.
        /// </param>
        /// <param name="az">
        /// Accelerometer z axis measurement in any calibrated units.
        /// </param>
        /// <param name="mx">
        /// Magnetometer x axis measurement in any calibrated units.
        /// </param>
        /// <param name="my">
        /// Magnetometer y axis measurement in any calibrated units.
        /// </param>
        /// <param name="mz">
        /// Magnetometer z axis measurement in any calibrated units.
        /// </param>
        /// <remarks>
        /// Optimised for minimal arithmetic.
        /// </remarks> 
        public void Update(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz)
        {
            float q1 = Quaternion[0], q2 = Quaternion[1], q3 = Quaternion[2], q4 = Quaternion[3];   // short name local variable for readability
            float norm;
            float hx, hy, bx, bz;
            float vx, vy, vz, wx, wy, wz;
            float ex, ey, ez;
            float pa, pb, pc;

            // Auxiliary variables to avoid repeated arithmetic
            float q1q1 = q1 * q1;
            float q1q2 = q1 * q2;
            float q1q3 = q1 * q3;
            float q1q4 = q1 * q4;
            float q2q2 = q2 * q2;
            float q2q3 = q2 * q3;
            float q2q4 = q2 * q4;
            float q3q3 = q3 * q3;
            float q3q4 = q3 * q4;
            float q4q4 = q4 * q4;

            // Normalise accelerometer measurement
            norm = (float)Math.Sqrt(ax * ax + ay * ay + az * az);
            if (norm == 0f) return; // handle NaN
            norm = 1 / norm;        // use reciprocal for division
            ax *= norm;
            ay *= norm;
            az *= norm;

            // Normalise magnetometer measurement
            norm = (float)Math.Sqrt(mx * mx + my * my + mz * mz);
            if (norm == 0f) return; // handle NaN
            norm = 1 / norm;        // use reciprocal for division
            mx *= norm;
            my *= norm;
            mz *= norm;

            // Reference direction of Earth's magnetic field
            hx = 2f * mx * (0.5f - q3q3 - q4q4) + 2f * my * (q2q3 - q1q4) + 2f * mz * (q2q4 + q1q3);
            hy = 2f * mx * (q2q3 + q1q4) + 2f * my * (0.5f - q2q2 - q4q4) + 2f * mz * (q3q4 - q1q2);
            bx = (float)Math.Sqrt((hx * hx) + (hy * hy));
            bz = 2f * mx * (q2q4 - q1q3) + 2f * my * (q3q4 + q1q2) + 2f * mz * (0.5f - q2q2 - q3q3);

            // Estimated direction of gravity and magnetic field
            vx = 2f * (q2q4 - q1q3);
            vy = 2f * (q1q2 + q3q4);
            vz = q1q1 - q2q2 - q3q3 + q4q4;
            wx = 2f * bx * (0.5f - q3q3 - q4q4) + 2f * bz * (q2q4 - q1q3);
            wy = 2f * bx * (q2q3 - q1q4) + 2f * bz * (q1q2 + q3q4);
            wz = 2f * bx * (q1q3 + q2q4) + 2f * bz * (0.5f - q2q2 - q3q3);

            // Error is cross product between estimated direction and measured direction of gravity
            ex = (ay * vz - az * vy) + (my * wz - mz * wy);
            ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
            ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
            if (Ki > 0f)
            {
                eInt[0] += ex;      // accumulate integral error
                eInt[1] += ey;
                eInt[2] += ez;
            }
            else
            {
                eInt[0] = 0.0f;     // prevent integral wind up
                eInt[1] = 0.0f;
                eInt[2] = 0.0f;
            }

            // Apply feedback terms
            gx = gx + Kp * ex + Ki * eInt[0];
            gy = gy + Kp * ey + Ki * eInt[1];
            gz = gz + Kp * ez + Ki * eInt[2];

            // Integrate rate of change of quaternion
            pa = q2;
            pb = q3;
            pc = q4;
            q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * SamplePeriod);
            q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * SamplePeriod);
            q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * SamplePeriod);
            q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * SamplePeriod);

            // Normalise quaternion
            norm = (float)Math.Sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
            norm = 1.0f / norm;
            Quaternion[0] = q1 * norm;
            Quaternion[1] = q2 * norm;
            Quaternion[2] = q3 * norm;
            Quaternion[3] = q4 * norm;
        }

        /// <summary>
        /// Algorithm IMU update method. Requires only gyroscope and accelerometer data.
        /// </summary>
        /// <param name="gx">
        /// Gyroscope x axis measurement in radians/s.
        /// </param>
        /// <param name="gy">
        /// Gyroscope y axis measurement in radians/s.
        /// </param>
        /// <param name="gz">
        /// Gyroscope z axis measurement in radians/s.
        /// </param>
        /// <param name="ax">
        /// Accelerometer x axis measurement in any calibrated units.
        /// </param>
        /// <param name="ay">
        /// Accelerometer y axis measurement in any calibrated units.
        /// </param>
        /// <param name="az">
        /// Accelerometer z axis measurement in any calibrated units.
        /// </param>
        public void Update(float gx, float gy, float gz, float ax, float ay, float az)
        {
            float q1 = Quaternion[0], q2 = Quaternion[1], q3 = Quaternion[2], q4 = Quaternion[3];   // short name local variable for readability
            float norm;
            float vx, vy, vz;
            float ex, ey, ez;
            float pa, pb, pc;

            // Normalise accelerometer measurement
            norm = (float)Math.Sqrt(ax * ax + ay * ay + az * az);
            if (norm == 0f) return; // handle NaN
            norm = 1 / norm;        // use reciprocal for division
            ax *= norm;
            ay *= norm;
            az *= norm;

            // Estimated direction of gravity
            vx = 2.0f * (q2 * q4 - q1 * q3);
            vy = 2.0f * (q1 * q2 + q3 * q4);
            vz = q1 * q1 - q2 * q2 - q3 * q3 + q4 * q4;

            // Error is cross product between estimated direction and measured direction of gravity
            ex = (ay * vz - az * vy);
            ey = (az * vx - ax * vz);
            ez = (ax * vy - ay * vx);
            if (Ki > 0f)
            {
                eInt[0] += ex;      // accumulate integral error
                eInt[1] += ey;
                eInt[2] += ez;
            }
            else
            {
                eInt[0] = 0.0f;     // prevent integral wind up
                eInt[1] = 0.0f;
                eInt[2] = 0.0f;
            }

            // Apply feedback terms
            gx = gx + Kp * ex + Ki * eInt[0];
            gy = gy + Kp * ey + Ki * eInt[1];
            gz = gz + Kp * ez + Ki * eInt[2];

            // Integrate rate of change of quaternion
            pa = q2;
            pb = q3;
            pc = q4;
            q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * SamplePeriod);
            q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * SamplePeriod);
            q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * SamplePeriod);
            q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * SamplePeriod);

            // Normalise quaternion
            norm = (float)Math.Sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
            norm = 1.0f / norm;
            Quaternion[0] = q1 * norm;
            Quaternion[1] = q2 * norm;
            Quaternion[2] = q3 * norm;
            Quaternion[3] = q4 * norm;
        }

		public float[] ConvertToConjugate()
		{
			float[] Q = { Quaternion[0], -Quaternion[1], -Quaternion[2], -Quaternion[3] };
			return Q;
		}

		public float[] GetRotationMatrix(float[] Quaternion)
        {
            float R11 = 2 * Quaternion[0] * Quaternion[0] - 1 + 2 * Quaternion[1] * Quaternion[1];
            float R12 = 2 * (Quaternion[1] * Quaternion[2] + Quaternion[0] * Quaternion[3]);
            float R13 = 2 * (Quaternion[1] * Quaternion[3] - Quaternion[0] * Quaternion[2]);
            float R21 = 2 * (Quaternion[1] * Quaternion[2] - Quaternion[0] * Quaternion[3]);
            float R22 = 2 * Quaternion[0] * Quaternion[0] - 1 + 2 * Quaternion[2] * Quaternion[2];
            float R23 = 2 * (Quaternion[2] * Quaternion[3] + Quaternion[0] * Quaternion[1]);
            float R31 = 2 * (Quaternion[1] * Quaternion[3] + Quaternion[0] * Quaternion[2]);
            float R32 = 2 * (Quaternion[2] * Quaternion[3] - Quaternion[0] * Quaternion[1]);
            float R33 = 2 * Quaternion[0] * Quaternion[0] - 1 + 2 * Quaternion[3] * Quaternion[3];
            return new float[] { R11, R12, R13,
                                 R21, R22, R23,
                                 R31, R32, R33 };
        }

        public float[] MultiplicationRotationMatrix(float[] R, float[] Acc)
        {
            float[] result = {
                R[0] * Acc[0] +  R[1] * Acc[1] +  R[2] * Acc[2],
                R[3] * Acc[0] +  R[4] * Acc[1] +  R[5] * Acc[2],
                R[6] * Acc[0] +  R[7] * Acc[1] +  R[8] * Acc[2],
            };
            return result;
        }

        public float[] CalculateLinearVelocyti(float[] tcAcc)
        {
            //Calculate linear acceleration in Earth frame (subtricting gravity)[]
            float[] linAcc = new float[3];
			///float min_level = 0.05f;
            //float[] linVel = new float[3];
            linAcc[0] = tcAcc[0] * 9.81f;
            linAcc[1] = tcAcc[1] * 9.81f;
            linAcc[2] = (tcAcc[2] ) * 9.81f;

            //Calculate linear velocity
            linVel[0] = prevLinVel[0] + linAcc[0] * SamplePeriod;
            linVel[1] = prevLinVel[1] + linAcc[1] * SamplePeriod;
            linVel[2] = prevLinVel[2] + linAcc[2] * SamplePeriod;

			//stationar stabilize
			//if (Math.Abs(tcAcc[0]) < min_level && Math.Abs(tcAcc[1]) < min_level && Math.Abs(tcAcc[2]) < min_level) {
				//gain = ;
				//linVel [0] = linVel [0] * (tcAcc[0] / min_level) ;
				//linVel [1] = linVel [1] * (tcAcc[1] / min_level) ;
				//linVel [2] = linVel [2] * (tcAcc[2] / min_level) ;

				//vel [1] = velY * accY * (1/(velX / min_level));
				//vel [2] = velZ * accZ * gain;
			//}

            prevLinVel = linVel;
            return linVel;
        }

		public float[] stationarStabilize( float velX, float velY, float velZ, float accX, float accY, float accZ, float min_level)
		{
			//float[] vel = new float[3];
			//float gain = 1.0f;
			if (Math.Abs(accX) < min_level && Math.Abs(accY) < min_level && Math.Abs(accZ) < min_level) {
				//gain = ;
				linVel [0] = velX * (accX / min_level) ;
				linVel [1] = velY * (accY / min_level) ;
				linVel [2] = velZ * (accZ / min_level) ;

				//vel [1] = velY * accY * (1/(velX / min_level));
				//vel [2] = velZ * accZ * gain;
			} else {
				linVel [0] = velX;
				linVel [1] = velY;
				linVel [2] = velZ;
			}

			return linVel;
		}

        //Hight-pass filter
		//See: http://habrahabr.ru/post/148326/
        //
		public float filterloop( float data, int index)
		{
			//int NZEROS = 1, NPOLES = 1;
            //float xv[NZEROS+1], yv[NPOLES+1];
			float GAIN = 1.726542528e+00F, OUR_GAIN = 1.0F;

			xv[index,0] = xv[index,1];
            xv[index, 1] = (float)data / GAIN;
            yv[index, 0] = yv[index, 1];
            yv[index, 1] = (xv[index, 1] - xv[index, 0]) + (0.1583844403F * yv[index, 0]);
            //return (float)((yv[index, 1] * OUR_GAIN) + (float)data);
			return (float)data;
		}

        //Calculate linear position
        public float[] CalculateLinPos(float[] linVelHP)
        {
            float[] linPos = new float[3];
            linPos[0] = prevLinVelHP[0] + linVelHP[0] * SamplePeriod;
            linPos[1] = prevLinVelHP[1] + linVelHP[1] * SamplePeriod;
            linPos[2] = prevLinVelHP[2] + linVelHP[2] * SamplePeriod;

            prevLinVelHP = linPos;
            return linPos;
        }

        public void ResetLinearPos() {
            prevLinVelHP[0] = 0f;
            prevLinVelHP[1] = 0f;
            prevLinVelHP[2] = 0f;
        }
    }
}

