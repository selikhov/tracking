using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

using ZedGraph;
using System.Drawing;



namespace Receiver
{
	public partial class Form2 : Form
	{

		int _capacity = 100;

		RollingPointPairList _dataX;
		RollingPointPairList _dataY;
		RollingPointPairList _dataZ;
		RollingPointPairList _dataQ;

		private double _valX;
		private double _valY;
		private double _valZ;
		private double _valQ;

		string _type;

		Sensors sensor;
		MahonyAHRS AHRS;
		MahonyAHRS AHRSfilt;

		// ????????? ???????????? ?????????
		double _amplitude = 1;
		//double _freq = 3;
		double _step = 0.1;

		// ??????? ???????? ?? ???????
		double _currentx = 0;
		float sampleRate = 1 / 40;

		public Form2 (string type)
		{
			_dataX = new RollingPointPairList (_capacity);
			_dataY = new RollingPointPairList (_capacity);
			_dataZ = new RollingPointPairList (_capacity);
			_dataQ = new RollingPointPairList (_capacity);

			_type = type;
			sensor = Sensors.getInstance();

			AHRS = new MahonyAHRS(1f / 40f, 3f);
			AHRSfilt = new MahonyAHRS (1f / 40f);

			InitializeComponent ();
			PrepareGraph ();
		}

		private void timer_Tick (object sender, EventArgs e)
		{
			Accelerometer acc = sensor.GetAccelObject();
			Gyroscope gyro = sensor.GetGyroscopeObject();
			Compas compas = sensor.GetCompasObject();
            Quaternion quat = sensor.GetQuaternion();
            //acc.customMedianFilter();

			int lenght = 512;
			Queue<double> QuFiltX = new Queue<double>(lenght);
			Queue<double> QuFiltY = new Queue<double>(lenght);
			Queue<double> QuFiltZ = new Queue<double>(lenght);
			double[] fft_complex = new double[lenght];
			double[] fft_real = new double[lenght];
			QuFiltX.Enqueue (acc.x);
			QuFiltY.Enqueue (acc.y);
			QuFiltZ.Enqueue (acc.z);

//			if (QuFiltX.Count == lenght) {
//				double[] ListX = QuFiltX.ToArray ();
//				double[] ListY = QuFiltY.ToArray ();
//				double[] ListZ = QuFiltZ.ToArray ();
//			
//				//double[] x = new double[]{ 1, 2, 3, 4 };
//				alglib.complex[] f;
//				alglib.fftr1d (ListX, out f);
//
//
//				for (int i = 0; i < f.Length; i++) {
//					fft_complex [i] = f [i].y;
//					fft_real [i] = f [i].x;
//					System.Console.WriteLine ("real - {0} complex - {1}", f [i].x, f [i].y);
//				}
//
//				System.Console.WriteLine ("{0}", alglib.ap.format (f, 3)); // EXPECTED: [10, -2+2i, -2, -2-2i]
//			}
           

           //float[] tcc = { (float)acc._acc_x, (float)acc._acc_y, (float)acc._acc_z };
           float[] vel = sensor.vel;
           //float[] linVel = sensor.linVel;
           float[] filtLinVel = sensor.filtLinVel;

            //�������� �������� �������� (����������� ���������)
             /*float[] vel = AHRSfilt.CalculateLinearVelocyti(tcc);
            //������������ ��������
            vel = AHRSfilt.stationarStabilize(vel[0], vel[1], vel[2], (float)acc._acc_x, (float)acc._acc_y, (float)acc._acc_z, 0.5f);
            
            float[] linVel = AHRSfilt.CalculateLinPos(vel);
            
            float[] filtLinVel = { 0f, 0f, 0f };
            filtLinVel[0] = AHRSfilt.filterloop(linVel[0], 0);
            filtLinVel[1] = AHRSfilt.filterloop(linVel[1], 1);
            filtLinVel[2] = AHRSfilt.filterloop(linVel[2], 2);
            
            //���������� ������ � Unity
            UdpSocket.Send(filtLinVel);*/
			switch(_type)
			{
			    case "accel":
				    _valX = (float)acc.x;
				    _valY = (float)acc.y;
				    _valZ = (float)acc.z;
				    break;
                case "acc":
                    _valX = (float)acc._acc_x;
                    _valY = (float)acc._acc_y;
                    _valZ = (float)acc._acc_z;
                    break;
                case "grav":
                    _valX = (float)acc._grav_x;
                    _valY = (float)acc._grav_y;
                    _valZ = (float)acc._grav_z;
                    break;
			    case "gyro":
				    _valX = gyro.x;
				    _valY = gyro.y;
				    _valZ = gyro.z;
				    break;
                case "mag":
                    _valX = compas.x;
                    _valY = compas.y;
                    _valZ = compas.z;
                    break;
			    case "quat_sensor":
                    _valX = quat.x;
                    _valY = quat.y;
                    _valZ = quat.z;
                    _valQ = quat.q;
                break;
				case "quat_math":
					AHRS.Update (
						deg2rad ((float)(gyro.x)), deg2rad ((float)(gyro.y)), deg2rad ((float)(gyro.z)), 
						(float)(acc.x), (float)(acc.y), (float)(acc.z), 
						(float)compas.x, (float)compas.y, (float)compas.z
					);
					float[] Q = AHRS.ConvertToConjugate ();
                    /*float[] R = AHRSfilt.GetRotationMatrix(AHRS.Quaternion);
                    float[] tcc = { (float)acc.x, (float)acc.y, (float)acc.z };
                    float[] tcAcc = AHRSfilt.MultiplicationRotationMatrix(R, tcc);*/
                    _valX = Q[1];
                    _valY = Q[2];
                    _valZ = Q[3];
                    _valQ = Q[0];
                break;
                case "vel":
                    //float[] q = { (float)quat.q, (float)quat.x, (float)quat.y, (float)quat.z };
                    //AHRS.Quaternion = q;
                    //AHRS.Quaternion = AHRSfilt.ConvertToConjugate (AHRS.Quaternion);
                    //float[] tcc = { (float)acc.x, (float)acc.y, (float)acc.z };
                    _valX = vel[0];
                    _valY = vel[1];
                    _valZ = vel[2];
                break;
                case "linvel":
                    _valX = filtLinVel[0];
                    _valY = filtLinVel[1];
                    _valZ = filtLinVel[2];
                break;
				case "fft_real":
					_valX = fft_real [0];
				break;
				case "fft_complex":
				_valX = (float)fft_complex [0];
				break;

			}

			// !!! ??????? ????? ?????? ? ??????
			_dataX.Add (_currentx, _valX);
			_dataY.Add (_currentx, _valY);
			_dataZ.Add (_currentx, _valZ);
			_dataQ.Add (_currentx, _valQ);

			_currentx += _step;

			// ?????????? ???????? ?? ??? X, ??????? ????? ?????????? ?? ???????
			double xmin = _currentx - _capacity * _step;
			double xmax = _currentx;

			GraphPane pane = zedGraph.GraphPane;
			pane.XAxis.Scale.Min = xmin;
			pane.XAxis.Scale.Max = xmax;

			// ??????? ???
			zedGraph.AxisChange ();

			// ??????? ??? ??????
			zedGraph.Invalidate ();
		}


		/// <summary>
		/// ?????????? ? ??????????? ??????
		/// </summary>
		private void PrepareGraph ()
		{
			// ??????? ?????? ??? ?????????
			GraphPane pane = zedGraph.GraphPane;
			pane.CurveList.Clear ();

			// ??????? ?????? ???? ??? ??? ?????-???? ?????
			pane.AddCurve ("Acc (x)", _dataX, Color.Blue, SymbolType.None);
			pane.AddCurve ("Acc (y)", _dataY, Color.Red, SymbolType.None);
			pane.AddCurve ("Acc (z)", _dataZ, Color.Green, SymbolType.None);

            if (_type == "quat_sensor" || _type == "quat_math")
				pane.AddCurve ("Acc (q)", _dataQ, Color.Black, SymbolType.None);

			// ????????????? ???????????? ??? ???????? ?? ??? Y
			pane.YAxis.Scale.Min = -_amplitude;
			pane.YAxis.Scale.Max = _amplitude;

			// ???????? ????? AxisChange (), ????? ???????? ?????? ?? ????. 
			zedGraph.AxisChange ();

			// ????????? ??????
			zedGraph.Invalidate ();
		}
		static float deg2rad(float degrees)
		{
			return (float)(Math.PI / 180) * degrees;
		}

	}
}
