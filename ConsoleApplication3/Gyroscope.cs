﻿using System;
using System.Collections.Generic;

namespace Receiver
{
	public class Gyroscope
	{
		public bool is_read = true;
		public double x;
		public double y;
		public double z;

		public void SetValueFromOscMessage(IList<object> Data)
		{
			double rrate_x = Data[0] is byte[] ? BitConverter.ToDouble((byte[])Data[0], 0) : Double.Parse(Data[0].ToString());
			double rrate_y = Data[1] is byte[] ? BitConverter.ToDouble((byte[])Data[1], 0) : Double.Parse(Data[1].ToString());
			double rrate_z = Data[2] is byte[] ? BitConverter.ToDouble((byte[])Data[2], 0) : Double.Parse(Data[2].ToString());

			x = Math.Round(rotation_to_degree(rrate_x), 5);
			y = Math.Round(rotation_to_degree(rrate_y), 5);
			z = Math.Round(rotation_to_degree(rrate_z), 5);

			this.is_read = false;
		}

		private double rotation_to_degree(double rrate)
		{
			return rrate * 180 / Math.PI;
		}
	}

}

