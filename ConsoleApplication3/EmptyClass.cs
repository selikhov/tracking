﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Receiver
{
	public class UdpSocket
	{
		private static IPAddress remoteIPAddress;
		private static int remotePort;
		private static int localPort;

		public static void Send(float[] data)//string datagram)
		{
			localPort = Convert.ToInt16(11000);
			remotePort = Convert.ToInt16(11000);
			remoteIPAddress = IPAddress.Parse("127.0.0.1");

			// Создаем UdpClient
			UdpClient sender = new UdpClient();

			// Создаем endPoint по информации об удаленном хосте
			IPEndPoint endPoint = new IPEndPoint(remoteIPAddress, remotePort);

			try
			{
				// Преобразуем данные в массив байтов
				byte[] bytes = new byte[data.Length * 4];
				Buffer.BlockCopy(data, 0, bytes, 0, bytes.Length);
				//byte[] bytes = Encoding.UTF8.GetBytes(datagram);

				// Отправляем данные
				sender.Send(bytes, bytes.Length, endPoint);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Возникло исключение: " + ex.ToString() + "\n  " + ex.Message);
			}
			finally
			{
				// Закрыть соединение
				sender.Close();
			}
		}

	}
}

